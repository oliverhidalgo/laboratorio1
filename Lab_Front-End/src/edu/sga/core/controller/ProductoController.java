package edu.sga.core.controller;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import edu.sga.core.bean.Producto;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.Gson;
import java.util.ArrayList;
public class ProductoController {
    private final String URL_API = "http://localhost:9002/api/v1/producto";
    private static ProductoController instancia;
    private Client productoCliente = ClientBuilder.newClient();
    public static ProductoController getInstancia(){
        if(instancia == null){
            instancia = new ProductoController();
        }
        return instancia;
    }
    public Producto getProductoNombre(String nombre){
        Producto resultado = null;
        WebTarget productoClienteTarget =
                productoCliente.target(URL_API + 
                        "?nombre=" + nombre);
            String json = productoClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        if(!json.isEmpty()){
            JsonParser parser = new JsonParser();
            JsonArray gsonArray = parser.parse(json)
                    .getAsJsonArray();
            for (JsonElement objeto : gsonArray) {
                resultado = new Gson().
                        fromJson(objeto,Producto.class);
            }                        
        }
        return resultado;
    }
    
    public List<Producto> getProductos(){
        WebTarget productoClienteTarget = 
                productoCliente.target(URL_API);
        String json =              
                productoClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        List<Producto> lista = new ArrayList<Producto>();
        JsonParser parser = new JsonParser();
        JsonArray gsonArray = parser.parse(json).getAsJsonArray();
        for (JsonElement objeto : gsonArray) {
           Producto elemento = new Gson().fromJson(objeto,Producto.class);
           lista.add(elemento);
        }
        return lista;
    }
    public void agregar(Producto elemento){
        WebTarget productoClienteTarget = 
                productoCliente.target(URL_API);
        productoClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .post(Entity.entity(new Gson().toJson(elemento),MediaType.APPLICATION_JSON));            
            
    }
    public void eliminar(Long id){
        WebTarget productoClienteTarget = 
                productoCliente.target(URL_API + "/" + id);
        productoClienteTarget.request(MediaType.APPLICATION_JSON).delete();
    }
    public void editar(Producto elemento){
         WebTarget productoClienteTarget = 
                productoCliente.target(URL_API + "/" + elemento.getCodigoProducto());       
        productoClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .put(Entity.entity(new Gson().toJson(elemento), MediaType.APPLICATION_JSON));
    }    
}