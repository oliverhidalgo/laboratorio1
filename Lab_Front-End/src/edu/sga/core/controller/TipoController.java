package edu.sga.core.controller;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import edu.sga.core.bean.Tipo;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.Gson;
import java.util.ArrayList;
public class TipoController {
    private final String URL_API = "http://localhost:9002/api/v1/tipo";
    private static TipoController instancia;
    private Client tipoCliente = ClientBuilder.newClient();
    public static TipoController getInstancia(){
        if(instancia == null){
            instancia = new TipoController();
        }
        return instancia;
    }
    public Tipo getTipoDescripcion(String descripcion){
        Tipo resultado = null;
        WebTarget tipoClienteTarget =
                tipoCliente.target(URL_API + 
                        "?descripcion=" + descripcion);
        String json = tipoClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        if(!json.isEmpty()){
            JsonParser parser = new JsonParser();
            JsonArray gsonArray = parser.parse(json)
                    .getAsJsonArray();
            for (JsonElement objeto : gsonArray) {
                resultado = new Gson().
                        fromJson(objeto,Tipo.class);
            }                        
        }
        return resultado;
    }
    public List<Tipo> getTipos(){
        WebTarget tipoClienteTarget = 
                tipoCliente.target(URL_API);
        String json =              
                tipoClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        List<Tipo> lista = new ArrayList<Tipo>();
        JsonParser parser = new JsonParser();
        JsonArray gsonArray = parser.parse(json).getAsJsonArray();
        for (JsonElement objeto : gsonArray) {
           Tipo elemento = new Gson().fromJson(objeto,Tipo.class);
           lista.add(elemento);
        }
        return lista;
    }
    public void agregar(Tipo elemento){
        WebTarget tipoClienteTarget = 
                tipoCliente.target(URL_API);
        tipoClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .post(Entity.entity(new Gson().toJson(elemento),MediaType.APPLICATION_JSON));            
            
    }
    public void eliminar(Long id){
        WebTarget tipoClienteTarget = 
                tipoCliente.target(URL_API + "/" + id);
        tipoClienteTarget.request(MediaType.APPLICATION_JSON).delete();
    }
    public void editar(Tipo elemento){
         WebTarget tipoClienteTarget = 
                tipoCliente.target(URL_API + "/" + elemento.getCodigoTipo());       
        tipoClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .put(Entity.entity(new Gson().toJson(elemento), MediaType.APPLICATION_JSON));
    }    
}