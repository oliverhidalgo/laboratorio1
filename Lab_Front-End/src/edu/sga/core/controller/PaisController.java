package edu.sga.core.controller;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import edu.sga.core.bean.Pais;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.Gson;
import java.util.ArrayList;
public class PaisController {
    private final String URL_API = "http://localhost:9002/api/v1/pais";
    private static PaisController instancia;
    private Client paisCliente = ClientBuilder.newClient();
    public static PaisController getInstancia(){
        if(instancia == null){
            instancia = new PaisController();
        }
        return instancia;
    }
    public Pais getPaisDescripcion(String descripcion){
        Pais resultado = null;
        WebTarget paisClienteTarget =
                paisCliente.target(URL_API + 
                        "?descripcion=" + descripcion);
        String json = paisClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        if(!json.isEmpty()){
            JsonParser parser = new JsonParser();
            JsonArray gsonArray = parser.parse(json)
                    .getAsJsonArray();
            for (JsonElement objeto : gsonArray) {
                resultado = new Gson().
                        fromJson(objeto,Pais.class);
            }                        
        }
        return resultado;
    }
    public List<Pais> getPaises(){
        WebTarget paisClienteTarget = 
                paisCliente.target(URL_API);
        String json =              
                paisClienteTarget
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
        List<Pais> lista = new ArrayList<Pais>();
        JsonParser parser = new JsonParser();
        JsonArray gsonArray = parser.parse(json).getAsJsonArray();
        for (JsonElement objeto : gsonArray) {
           Pais elemento = new Gson().fromJson(objeto,Pais.class);
           lista.add(elemento);
        }
        return lista;
    }
    public void agregar(Pais elemento){
        WebTarget paisClienteTarget = 
                paisCliente.target(URL_API);
        paisClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .post(Entity.entity(new Gson().toJson(elemento),MediaType.APPLICATION_JSON));            
            
    }
    public void eliminar(Long id){
        WebTarget paisClienteTarget = 
                paisCliente.target(URL_API + "/" + id);
        paisClienteTarget.request(MediaType.APPLICATION_JSON).delete();
    }
    public void editar(Pais elemento){
         WebTarget paisClienteTarget = 
                paisCliente.target(URL_API + "/" + elemento.getCodigoPais());       
        paisClienteTarget
          .request(MediaType.APPLICATION_JSON)
          .put(Entity.entity(new Gson().toJson(elemento), MediaType.APPLICATION_JSON));
    }    
}