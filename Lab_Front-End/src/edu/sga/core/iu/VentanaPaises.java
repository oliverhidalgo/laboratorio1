package edu.sga.core.iu;
import edu.sga.core.bean.Pais;
import edu.sga.core.controller.PaisController;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import edu.sga.core.model.PaisModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
public class VentanaPaises extends JFrame {
    private JTable tblPaises;
    private JScrollPane jcPaises;
    private JButton btnAgregar;
    private JButton btnEliminar;
    private JButton btnEditar;    
    private PaisModel modelo;
    public VentanaPaises() {
        this.setTitle("Paises");
        this.setSize(800,600);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tblPaises = new JTable();
        modelo = new PaisModel();
        tblPaises.setModel(modelo);
        jcPaises = new JScrollPane();
        jcPaises.setViewportView(tblPaises);
        jcPaises.setBounds(5,5,775,500);
        btnAgregar = new JButton("Agregar");
        btnEliminar = new JButton("Eliminar");
        btnEditar = new JButton("Editar");
        btnEditar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblPaises.getSelectedRow() != -1){
                    Pais elemento = modelo.getPais(tblPaises.getSelectedRow());
                    String descripcion = JOptionPane.showInputDialog(null,"Ingrese una descripcion",elemento.getDescripcion());
                    elemento.setDescripcion(descripcion);
                    modelo.editar(tblPaises.getSelectedRow(),elemento);
                    JOptionPane.showMessageDialog(null,"Pais modificado");
                }else{
                    JOptionPane.showMessageDialog(null
                            ,"Debe seleccionar un elemento"
                            ,"Editar"
                            ,JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnAgregar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String descripcion = JOptionPane
                        .showInputDialog("Descripcion:");
                if(!descripcion.isEmpty()){
                    Pais pais = new Pais();
                    pais.setDescripcion(descripcion);
                    modelo.agregar(pais);
                    JOptionPane.showMessageDialog(null
                            ,"Pais agregado"
                            ,"Paises"
                            ,JOptionPane.INFORMATION_MESSAGE);
                }else{
                        JOptionPane.showMessageDialog(null
                            ,"Debe agregar una descripción"
                            ,"Paises"
                            ,JOptionPane.ERROR_MESSAGE);
                }
                
            }
        });
        btnEliminar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblPaises.getSelectedRow() != -1){
                    int respuesta = JOptionPane
                            .showConfirmDialog(null
                              ,"Esta seguro de eliminar el registro?"
                              ,"Eliminar"
                              ,JOptionPane.YES_NO_OPTION);
                    if(respuesta == JOptionPane.YES_OPTION){
                        Pais elemento = modelo.
                            getPais(tblPaises.getSelectedRow());
                        modelo.eliminar(elemento);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,
                            "Debe seleccionar un elemento");
                }
            }
        });
        btnAgregar.setBounds(5,515,120,40);
        btnEliminar.setBounds(130,515,120,40);
        btnEditar.setBounds(255,515,120,40);
        getContentPane().add(jcPaises);
        getContentPane().add(btnAgregar);
        getContentPane().add(btnEliminar);
        getContentPane().add(btnEditar);
        this.setVisible(true);
    }
    
}
