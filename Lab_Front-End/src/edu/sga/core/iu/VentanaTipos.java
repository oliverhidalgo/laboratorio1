package edu.sga.core.iu;
import edu.sga.core.bean.Tipo;
import edu.sga.core.controller.TipoController;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import edu.sga.core.model.TipoModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
public class VentanaTipos extends JFrame {
    private JTable tblTipos;
    private JScrollPane jcTipos;
    private JButton btnAgregar;
    private JButton btnEliminar;
    private JButton btnEditar;    
    private TipoModel modelo;
    public VentanaTipos() {
        this.setTitle("Tipos");
        this.setSize(800,600);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tblTipos = new JTable();
        modelo = new TipoModel();
        tblTipos.setModel(modelo);
        jcTipos = new JScrollPane();
        jcTipos.setViewportView(tblTipos);
        jcTipos.setBounds(5,5,775,500);
        btnAgregar = new JButton("Agregar");
        btnEliminar = new JButton("Eliminar");
        btnEditar = new JButton("Editar");
        btnEditar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblTipos.getSelectedRow() != -1){
                    Tipo elemento = modelo.getTipo(tblTipos.getSelectedRow());
                    String descripcion = JOptionPane.showInputDialog(null,"Ingrese una descripcion",elemento.getDescripcion());
                    elemento.setDescripcion(descripcion);
                    modelo.editar(tblTipos.getSelectedRow(),elemento);
                    JOptionPane.showMessageDialog(null,"Tipo modificado");
                }else{
                    JOptionPane.showMessageDialog(null
                            ,"Debe seleccionar un elemento"
                            ,"Editar"
                            ,JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnAgregar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String descripcion = JOptionPane
                        .showInputDialog("Descripcion:");
                if(!descripcion.isEmpty()){
                    Tipo tipo = new Tipo();
                    tipo.setDescripcion(descripcion);
                    modelo.agregar(tipo);
                    JOptionPane.showMessageDialog(null
                            ,"Tipo agregado"
                            ,"Tipos"
                            ,JOptionPane.INFORMATION_MESSAGE);
                }else{
                        JOptionPane.showMessageDialog(null
                            ,"Debe agregar una descripción"
                            ,"Tipos"
                            ,JOptionPane.ERROR_MESSAGE);
                }
                
            }
        });
        btnEliminar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblTipos.getSelectedRow() != -1){
                    int respuesta = JOptionPane
                            .showConfirmDialog(null
                              ,"Esta seguro de eliminar el registro?"
                              ,"Eliminar"
                              ,JOptionPane.YES_NO_OPTION);
                    if(respuesta == JOptionPane.YES_OPTION){
                        Tipo elemento = modelo.
                            getTipo(tblTipos.getSelectedRow());
                        modelo.eliminar(elemento);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,
                            "Debe seleccionar un elemento");
                }
            }
        });
        btnAgregar.setBounds(5,515,120,40);
        btnEliminar.setBounds(130,515,120,40);
        btnEditar.setBounds(255,515,120,40);
        getContentPane().add(jcTipos);
        getContentPane().add(btnAgregar);
        getContentPane().add(btnEliminar);
        getContentPane().add(btnEditar);
        this.setVisible(true);
    }
    
}
