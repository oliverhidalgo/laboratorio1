package edu.sga.core.iu;
import edu.sga.core.bean.Pais;
import edu.sga.core.bean.Producto;
import edu.sga.core.bean.Tipo;
import edu.sga.core.controller.PaisController;
import edu.sga.core.controller.ProductoController;
import edu.sga.core.controller.TipoController;
import edu.sga.core.model.PaisModel;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import edu.sga.core.model.ProductoModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
public class VentanaProductos extends JFrame {
    private JTable tblProductos;
    private JScrollPane jcProductos;
    private JButton btnAgregar;
    private JButton btnEliminar;
    private JButton btnEditar;    
    private ProductoModel modelo;
    public VentanaProductos() {
        this.setTitle("Productos");
        this.setSize(800,600);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tblProductos = new JTable();
        modelo = new ProductoModel();
        tblProductos.setModel(modelo);
        jcProductos = new JScrollPane();
        jcProductos.setViewportView(tblProductos);
        jcProductos.setBounds(5,5,775,500);
        btnAgregar = new JButton("Agregar");
        btnEliminar = new JButton("Eliminar");
        btnEditar = new JButton("Editar");
        btnEditar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblProductos.getSelectedRow() != -1){
                    Producto elemento = modelo.getProducto(tblProductos.getSelectedRow());
                    String nombre = JOptionPane.showInputDialog(null,"Ingrese un nombre",elemento.getNombre());
                    elemento.setNombre(nombre);
                    modelo.editar(tblProductos.getSelectedRow(),elemento);
                    JOptionPane.showMessageDialog(null,"Producto modificado");
                }else{
                    JOptionPane.showMessageDialog(null
                            ,"Debe seleccionar un elemento"
                            ,"Editar"
                            ,JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnAgregar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                PaisController pais = new PaisController();
                TipoController tipo = new TipoController();
                List<Pais> lista = new ArrayList<Pais>();
                List<Tipo> listaTipo = new ArrayList<Tipo>();
                lista = pais.getPaises();
                listaTipo = tipo.getTipos();
                Long paisIndex = 0L;
                Long tipoIndex = 0L;
               
                JComboBox<String> cmbPais = new JComboBox<String>();
                JComboBox<String> cmbTipo = new JComboBox<String>();
                cmbPais.setEditable(true);
                cmbTipo.setEditable(true);
                
                for(Pais p : lista){
                    cmbPais.addItem(p.getDescripcion());
                }
                
                for(Tipo t : listaTipo){
                    cmbTipo.addItem(t.getDescripcion());
                }
                
                String mesGarantia = "";
                do{
                mesGarantia = JOptionPane
                        .showInputDialog("Meses de Garantia:");
                }while(mesGarantia == "");
                String nombre = JOptionPane
                        .showInputDialog("Nombre:");
                String precio = JOptionPane
                        .showInputDialog("Precio:");
                JOptionPane.showMessageDialog(null, cmbPais, "Selecciones un Pais", JOptionPane.QUESTION_MESSAGE);
                JOptionPane.showMessageDialog(null, cmbTipo, "Selecciones un Tipo", JOptionPane.QUESTION_MESSAGE);
                
                for(Pais p : lista){
                    if(cmbPais.getSelectedItem() == p.getDescripcion()){
                        paisIndex = p.getCodigoPais();
                    }
                }
                
                for(Tipo t : listaTipo){
                    if(cmbTipo.getSelectedItem() == t.getDescripcion()){
                        tipoIndex = t.getCodigoTipo();
                    }
                }
                
                if(!nombre.isEmpty() && !mesGarantia.isEmpty() && !precio.isEmpty() && cmbPais.getSelectedItem() != null && cmbTipo.getSelectedItem() != null){
                    Producto producto = new Producto();
                    Pais paisElement = new Pais();
                    Tipo tipoElement = new Tipo();
                    paisElement.setCodigoPais(paisIndex);
                    paisElement.setDescripcion(cmbPais.getSelectedItem().toString());
                    tipoElement.setCodigoTipo(tipoIndex);
                    tipoElement.setDescripcion(cmbTipo.getSelectedItem().toString());
                    producto.setNombre(nombre);
                    producto.setGarantiaMes(Integer.parseInt(mesGarantia));
                    producto.setPrecio(Float.parseFloat(precio));
                    producto.setPais(paisElement);
                    producto.setTipo(tipoElement);
                    System.out.println("Objeto recien guardado " + producto.getCodigoProducto() + " " + producto.getGarantiaMes() + " " + producto.getPrecio() + " " + producto.getNombre() 
                            + " " + producto.getPais().getDescripcion() + " " + producto.getTipo().getDescripcion());
                    modelo.agregar(producto);
                    System.out.println("");
                    JOptionPane.showMessageDialog(null
                            ,"Producto agregado"
                            ,"Productos"
                            ,JOptionPane.INFORMATION_MESSAGE);
                }else if(nombre.isEmpty()){
                        JOptionPane.showMessageDialog(null
                            ,"Debe agregar un nombre"
                            ,"Productos"
                            ,JOptionPane.ERROR_MESSAGE);
                }else if(mesGarantia.isEmpty()){
                        JOptionPane.showMessageDialog(null
                            ,"Debe agregar meses de garantia"
                            ,"Productos"
                            ,JOptionPane.ERROR_MESSAGE);
                }else if(precio.isEmpty()){
                        JOptionPane.showMessageDialog(null
                            ,"Debe agregar precio"
                            ,"Productos"
                            ,JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        btnEliminar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(tblProductos.getSelectedRow() != -1){
                    int respuesta = JOptionPane
                            .showConfirmDialog(null
                              ,"Esta seguro de eliminar el registro?"
                              ,"Eliminar"
                              ,JOptionPane.YES_NO_OPTION);
                    if(respuesta == JOptionPane.YES_OPTION){
                        Producto elemento = modelo.
                            getProducto(tblProductos.getSelectedRow());
                        modelo.eliminar(elemento);
                    }
                }else{
                    JOptionPane.showMessageDialog(null,
                            "Debe seleccionar un elemento");
                }
            }
        });
        btnAgregar.setBounds(5,515,120,40);
        btnEliminar.setBounds(130,515,120,40);
        btnEditar.setBounds(255,515,120,40);
        getContentPane().add(jcProductos);
        getContentPane().add(btnAgregar);
        getContentPane().add(btnEliminar);
        getContentPane().add(btnEditar);
        this.setVisible(true);
    }
    
}
