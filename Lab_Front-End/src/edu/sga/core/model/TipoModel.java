package edu.sga.core.model;
import javax.swing.table.AbstractTableModel;
import java.util.List;
import edu.sga.core.bean.Tipo;
import edu.sga.core.controller.TipoController;
public class TipoModel extends AbstractTableModel {
    //Encabezado de la tabla
    private String encabezados[] = {"Codigo","Tipo"};
    //Obtener la informacion de tipos en el API REST
    private List<Tipo> lista = TipoController.getInstancia().getTipos();
    @Override
    public int getRowCount() {
        return lista.size();
    }
    @Override
    public int getColumnCount() {
        return encabezados.length;
    }

    public String getColumnName(int columna){
        return encabezados[columna];
    }
    
    @Override
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Tipo elemento = lista.get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(elemento.getCodigoTipo());
                break;
            case 1:
                resultado = elemento.getDescripcion();
                break;
        }
        return resultado;
    }
    public Tipo getTipo(int fila){
        return lista.get(fila);
    }
    public void agregar(Tipo tipo){
        try{
            TipoController.getInstancia().agregar(tipo);
            tipo = TipoController.getInstancia()
                    .getTipoDescripcion(
                            tipo.getDescripcion());
            lista.add(tipo);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void eliminar(Tipo tipo){
        try{
            TipoController.getInstancia()
                .eliminar(tipo.getCodigoTipo());
            lista.remove(tipo);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void editar(int posicion,Tipo tipo){
        try{
            TipoController.getInstancia().editar(tipo);
            lista.set(posicion,tipo);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}


