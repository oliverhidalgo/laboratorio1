package edu.sga.core.model;
import javax.swing.table.AbstractTableModel;
import java.util.List;
import edu.sga.core.bean.Producto;
import edu.sga.core.controller.ProductoController;
public class ProductoModel extends AbstractTableModel {
    //Encabezado de la tabla
    private String encabezados[] = {"Codigo","Meses de Garantia", "Producto", "Precio", "Pais", "Tipo"};
    //Obtener la informacion de productos en el API REST
    private List<Producto> lista = ProductoController.getInstancia().getProductos();
    @Override
    public int getRowCount() {
        return lista.size();
    }
    @Override
    public int getColumnCount() {
        return encabezados.length;
    }

    public String getColumnName(int columna){
        return encabezados[columna];
    }
    
    @Override
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Producto elemento = lista.get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(elemento.getCodigoProducto());
                break;
            case 1:
                resultado = String.valueOf(elemento.getGarantiaMes());
                break;
            case 2:
                resultado = elemento.getNombre();
                break;
            case 3:
                resultado = String.valueOf(elemento.getPrecio());
                break;
            case 4:
                resultado = String.valueOf(elemento.getPais().getDescripcion());
                break; 
            case 5:
                resultado = String.valueOf(elemento.getTipo().getDescripcion());
                break; 
        }
        return resultado;
    }
    public Producto getProducto(int fila){
        return lista.get(fila);
    }
    public void agregar(Producto producto){
        try{
            ProductoController.getInstancia().agregar(producto);
            producto = ProductoController.getInstancia()
                    .getProductoNombre(
                            producto.getNombre());
            lista.add(producto);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void eliminar(Producto producto){
        try{
            ProductoController.getInstancia()
                .eliminar(producto.getCodigoProducto());
            lista.remove(producto);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void editar(int posicion,Producto producto){
        try{
            ProductoController.getInstancia().editar(producto);
            lista.set(posicion,producto);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
}


