package edu.sga.core.model;
import javax.swing.table.AbstractTableModel;
import java.util.List;
import edu.sga.core.bean.Pais;
import edu.sga.core.controller.PaisController;
public class PaisModel extends AbstractTableModel {
    //Encabezado de la tabla
    private String encabezados[] = {"Codigo","Pais"};
    //Obtener la informacion de paises en el API REST
    private List<Pais> lista = PaisController.getInstancia().getPaises();
    @Override
    public int getRowCount() {
        return lista.size();
    }
    @Override
    public int getColumnCount() {
        return encabezados.length;
    }

    public String getColumnName(int columna){
        return encabezados[columna];
    }
    
    @Override
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Pais elemento = lista.get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(elemento.getCodigoPais());
                break;
            case 1:
                resultado = elemento.getDescripcion();
                break;
        }
        return resultado;
    }
    public Pais getPais(int fila){
        return lista.get(fila);
    }
    public void agregar(Pais carrera){
        try{
            PaisController.getInstancia().agregar(carrera);
            carrera = PaisController.getInstancia()
                    .getPaisDescripcion(
                            carrera.getDescripcion());
            lista.add(carrera);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void eliminar(Pais carrera){
        try{
            PaisController.getInstancia()
                .eliminar(carrera.getCodigoPais());
            lista.remove(carrera);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void editar(int posicion,Pais carrera){
        try{
            PaisController.getInstancia().editar(carrera);
            lista.set(posicion,carrera);
            fireTableDataChanged();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}


