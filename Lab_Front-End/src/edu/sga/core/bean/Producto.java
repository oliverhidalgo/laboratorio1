 
package edu.sga.core.bean;
import java.io.Serializable;
public class Producto implements Serializable {
   private Long codigoProducto;
   private int garantiaMes;
   private String nombre;
   private float precio;
   private Pais pais;
   private Tipo tipo;

    public int getGarantiaMes() {
        return garantiaMes;
    }

    public void setGarantiaMes(int garantiaMes) {
        this.garantiaMes = garantiaMes;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
   
   public Producto() {
   }
    public Producto(Long codigoProducto, String nombre) {
        this.codigoProducto = codigoProducto;
        this.nombre = nombre;
    }

    public Long getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(Long codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
}
