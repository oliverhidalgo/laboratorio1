 
package edu.sga.core.bean;
import java.io.Serializable;
public class Tipo implements Serializable {
   private Long codigoTipo;
   private String descripcion;
   public Tipo() {
   }
    public Tipo(Long codigoTipo, String descripcion) {
        this.codigoTipo = codigoTipo;
        this.descripcion = descripcion;
    }

    public Long getCodigoTipo() {
        return codigoTipo;
    }

    public void setCodigoTipo(Long codigoTipo) {
        this.codigoTipo = codigoTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
}
