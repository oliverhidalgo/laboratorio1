 
package edu.sga.core.bean;
import java.io.Serializable;
public class Pais implements Serializable {
   private Long codigoPais;
   private String descripcion;
   public Pais() {
   }
    public Pais(Long codigoPais, String descripcion) {
        this.codigoPais = codigoPais;
        this.descripcion = descripcion;
    }

    public Long getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(Long codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
}
