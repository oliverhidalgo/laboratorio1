package edu.sga.core.sistema;
import edu.sga.core.iu.VentanaPaises;
import edu.sga.core.iu.VentanaProductos;
import edu.sga.core.iu.VentanaTipos;
import javax.swing.JOptionPane;

public class Principal {
    public static void main(String args[]){        
        String op = JOptionPane.showInputDialog("Bienvenido al sistema de productos musicales\nEscoja una opcion:\n1. Ventana Tipos \n2. Ventana Paises\n3. Ventana Productos", "");
        if(op.equals("1")){
            new VentanaTipos();
        }
        if(op.equals("2")){
            new VentanaPaises();
        }
        if(op.equals("3")){
            new VentanaProductos();
        }
        
    }
}
