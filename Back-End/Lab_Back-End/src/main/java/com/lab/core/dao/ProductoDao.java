package com.lab.core.dao;

import java.util.List;

import com.lab.core.model.Producto;

public interface ProductoDao {
	void saveProducto(Producto elemento);
	void deleteProductoById(Long codigoProducto);
	void updateProducto(Producto elemento);
	List<Producto> findAllProductos();
	Producto findById(Long codigoProducto);
	Producto findByNombre(String nombre);
}
