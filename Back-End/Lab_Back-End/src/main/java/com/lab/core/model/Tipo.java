package com.lab.core.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tipo")
public class Tipo implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo_tipo")
	private Long codigoTipo;
	@Column(name="descripcion")	
	private String descripcion;
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER,mappedBy="tipo",cascade = CascadeType.PERSIST)
	private Set<Producto> producto = new HashSet<Producto>();
	
	public Tipo() {
		// TODO Auto-generated constructor stub
	}
	public Tipo(Long codigoTipo, String descripcion, Set<Producto> producto) {
		super();
		this.codigoTipo = codigoTipo;
		this.descripcion = descripcion;
		this.producto = producto;
	}
	public Long getCodigoTipo() {
		return codigoTipo;
	}
	public void setCodigoTipo(Long codigoTipo) {
		this.codigoTipo = codigoTipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Set<Producto> getProducto() {
		return producto;
	}
	public void setProducto(Set<Producto> producto) {
		this.producto = producto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((codigoTipo == null) ? 0 : codigoTipo.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tipo other = (Tipo) obj;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (codigoTipo == null) {
			if (other.codigoTipo != null)
				return false;
		} else if (!codigoTipo.equals(other.codigoTipo))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}
	

}
