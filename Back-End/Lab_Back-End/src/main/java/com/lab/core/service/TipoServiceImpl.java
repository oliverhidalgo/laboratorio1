package com.lab.core.service;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lab.core.dao.PaisDao;
import com.lab.core.dao.TipoDao;
import com.lab.core.model.Pais;
import com.lab.core.model.Tipo;
@Service("tipoService")
@Transactional
public class TipoServiceImpl implements TipoService{
	@Autowired
	private TipoDao tipoServiceDao;

	@Override
	public void saveTipo(Tipo elemento) {
		tipoServiceDao.saveTipo(elemento);
		
	}

	@Override
	public void deleteTipoById(Long codigoTipo) {
		tipoServiceDao.deleteTipoById(codigoTipo);
		
	}

	@Override
	public void updateTipo(Tipo elemento) {
		tipoServiceDao.updateTipo(elemento);
		
	}

	@Override
	public List<Tipo> findAllTipos() {
		return tipoServiceDao.findAllTipos();
	}

	@Override
	public Tipo findById(Long codigoTipo) {
		return tipoServiceDao.findById(codigoTipo);
	}

	@Override
	public Tipo findByDescripcion(String descripcion) {
		return tipoServiceDao.findByDescripcion(descripcion);
	}
}
