package com.lab.core.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.lab.core.model.Pais;
@Repository
@Transactional
public class PaisDaoImpl extends AbstractSession 
	implements PaisDao {
	@Override
	public void savePais(Pais elemento) {
		getSession().persist(elemento);		
	}
	@Override
	public void deletePaisById(Long codigoPais) {
		Pais elemento = findById(codigoPais);
		if(elemento != null) {
			getSession().delete(elemento);
		}		
	}
	@Override
	public void updatePais(Pais elemento) {
		getSession().update(elemento);
	}
	@Override
	public List<Pais> findAllPaises() {
		return getSession().createQuery("from Pais").list();
	}
	@Override
	public Pais findById(Long codigoPais) {
		return (Pais)getSession().get(Pais.class,codigoPais);
	}
	@Override
	public Pais findByDescripcion(String descripcion) {
		return (Pais) getSession()
				.createQuery("from Pais where descripcion = :descripcion")
				.setParameter("descripcion",descripcion)
				.uniqueResult();
	}

}
