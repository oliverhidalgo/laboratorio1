package com.lab.core.service;
import java.util.List;

import com.lab.core.model.Producto;


public interface ProductoService {
	void saveProducto(Producto elemento);
	void deleteProductoById(Long codigoProducto);
	void updateProducto(Producto elemento);
	List<Producto> findAllProductos();
	Producto findById(Long codigoProducto);
	Producto findByNombre(String nombre);
}
