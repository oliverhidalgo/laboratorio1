package com.lab.core.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="producto")
public class Producto implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="codigo_producto")
	private Long codigoProducto;
	@Column(name ="garantia_mes")
	private int garantiaMes;
	@Column(name ="nombre")
	private String nombre;
	@Column(name ="precio")
	private float precio;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name ="codigo_pais")
	private Pais pais;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name ="codigo_tipo")
	private Tipo tipo;
	
	
	public Producto() {
		// TODO Auto-generated constructor stub
	}
	public Producto(Long codigoProducto, int garantiaMes, String nombre, float precio, 
			Pais pais, Tipo tipo) {
		super();
		this.codigoProducto = codigoProducto;
		this.garantiaMes = garantiaMes;
		this.nombre = nombre;
		this.precio = precio;
		this.pais = pais;
		this.tipo = tipo;
	}
	public Long getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(Long codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public int getGarantiaMes() {
		return garantiaMes;
	}
	public void setGarantiaMes(int garantiaMes) {
		this.garantiaMes = garantiaMes;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	
}
