package com.lab.core.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import com.lab.core.model.Pais;
import com.lab.core.service.PaisService;


@Controller
@RequestMapping("/api/v1")

public class PaisController {
	@Autowired
	private PaisService paisService;
	@RequestMapping(value="/pais",method= RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<List<Pais>> getPaises(
			@RequestParam(value="descripcion",
			required = false)String descripcion){
		List<Pais> pais = new ArrayList<Pais>();
		if(descripcion != null) {
			Pais elemento = paisService.findByDescripcion(descripcion);
			if(elemento == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}else {
				pais.add(elemento);
			}
		}else {
			pais = paisService.findAllPaises();
			if(pais.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
		}
		return new ResponseEntity<List<Pais>>(pais,HttpStatus.OK);
	}
	//GET FINDBYID
	@RequestMapping(value="/pais/{id}",method=RequestMethod.GET,headers="Accept=application/json")
		public ResponseEntity<Pais> getPaisById(@PathVariable("id")Long id){
			Pais elemento = paisService.findById(id);
			if(elemento == null) {
				return new ResponseEntity<Pais>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Pais>(elemento,HttpStatus.OK);
	}	
	//POST
	@RequestMapping(value="/pais",method = RequestMethod.POST,headers="Accept=application/json")
	public ResponseEntity<?> createPais(@RequestBody
			Pais pais,UriComponentsBuilder ucBuilder){
		if(paisService.findByDescripcion(pais.getDescripcion())!=null){
		return new ResponseEntity("Existe un pais con esta descripcion",HttpStatus.CONFLICT);
		
	}
	paisService.savePais(pais);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder
			.path("api/v1/pais/{id}")
			.buildAndExpand(pais.getCodigoPais())
			.toUri());
	return new ResponseEntity<String>(headers,HttpStatus.CREATED);

	}
	//DELETE
	@RequestMapping(value="/pais/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<?> deletePais(@PathVariable("id") Long id){
		if(id == null || id <= 0) {
			return new ResponseEntity("Debe ingresar un id",HttpStatus.CONFLICT);
		}
		Pais elemento = paisService.findById(id);
		if(elemento == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		paisService.deletePaisById(elemento.getCodigoPais());
		return new ResponseEntity<Pais>(HttpStatus.OK);	
	}
	
	//UPDATE
	@RequestMapping(value="/pais/{id}", method = RequestMethod.PUT,headers="Accept=application/json")
	public ResponseEntity<Pais> updatePais(@PathVariable("id") Long id,@RequestBody Pais pais){
		if(id == null || id <=0) {
			return new ResponseEntity("Debe Ingresar un id",HttpStatus.CONFLICT);
		}
		Pais elemento = paisService.findById(id);
		if(elemento == null) {
			return new ResponseEntity("",HttpStatus.NO_CONTENT);
		}
		elemento.setDescripcion(pais.getDescripcion());
		paisService.updatePais(elemento);
		return new ResponseEntity<Pais>(elemento,HttpStatus.OK);
	}
}
 
