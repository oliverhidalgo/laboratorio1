package com.lab.core.dao;
import java.util.List;

import com.lab.core.model.Pais;
import com.lab.core.model.Tipo;
public interface TipoDao {
	void saveTipo(Tipo elemento);
	void deleteTipoById(Long codigoTipo);
	void updateTipo(Tipo elemento);
	List<Tipo> findAllTipos();
	Tipo findById(Long codigoTipo);
	Tipo findByDescripcion(String descripcion);
}
