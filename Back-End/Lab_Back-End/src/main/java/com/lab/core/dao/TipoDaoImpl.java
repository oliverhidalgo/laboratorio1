package com.lab.core.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.lab.core.model.Pais;
import com.lab.core.model.Tipo;
@Repository
@Transactional
public class TipoDaoImpl extends AbstractSession 
	implements TipoDao {
	@Override
	public void saveTipo(Tipo elemento) {
		getSession().persist(elemento);		
	}
	@Override
	public void deleteTipoById(Long codigoTipo) {
		Tipo elemento = findById(codigoTipo);
		if(elemento != null) {
			getSession().delete(elemento);
		}		
	}
	@Override
	public void updateTipo(Tipo elemento) {
		getSession().update(elemento);
	}
	@Override
	public List<Tipo> findAllTipos() {
		return getSession().createQuery("from Tipo").list();
	}
	@Override
	public Tipo findById(Long codigoTipo) {
		return (Tipo)getSession().get(Tipo.class,codigoTipo);
	}
	@Override
	public Tipo findByDescripcion(String descripcion) {
		return (Tipo) getSession()
				.createQuery("from Tipo where descripcion = :descripcion")
				.setParameter("descripcion",descripcion)
				.uniqueResult();
	}

}
