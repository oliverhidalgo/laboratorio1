package com.lab.core.dao;
import java.util.List;

import com.lab.core.model.Pais;
public interface PaisDao {
	void savePais(Pais elemento);
	void deletePaisById(Long codigoPais);
	void updatePais(Pais elemento);
	List<Pais> findAllPaises();
	Pais findById(Long codigoPais);
	Pais findByDescripcion(String descripcion);
}
