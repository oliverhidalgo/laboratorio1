package com.lab.core.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="pais")
public class Pais implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="codigo_pais")
	private Long codigoPais;
	@Column(name="descripcion")	
	private String descripcion;
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER,mappedBy="pais",cascade = CascadeType.PERSIST)
	private Set<Producto> producto = new HashSet<Producto>();
	
	public Pais() {
		// TODO Auto-generated constructor stub
	}
	public Pais(Long codigoPais, String descripcion, Set<Producto> producto) {
		super();
		this.codigoPais = codigoPais;
		this.descripcion = descripcion;
		this.producto = producto;
	}
	public Long getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(Long codigoPais) {
		this.codigoPais = codigoPais;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Set<Producto> getProducto() {
		return producto;
	}
	public void setProducto(Set<Producto> producto) {
		this.producto = producto;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		result = prime * result + ((codigoPais == null) ? 0 : codigoPais.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		if (codigoPais == null) {
			if (other.codigoPais != null)
				return false;
		} else if (!codigoPais.equals(other.codigoPais))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}
	

}
