package com.lab.core.service;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lab.core.dao.PaisDao;
import com.lab.core.model.Pais;
@Service("paisService")
@Transactional
public class PaisServiceImpl implements PaisService{
	@Autowired
	private PaisDao paisServiceDao;

	@Override
	public void savePais(Pais elemento) {
		paisServiceDao.savePais(elemento);
		
	}

	@Override
	public void deletePaisById(Long codigoPais) {
		paisServiceDao.deletePaisById(codigoPais);
		
	}

	@Override
	public void updatePais(Pais elemento) {
		paisServiceDao.updatePais(elemento);
		
	}

	@Override
	public List<Pais> findAllPaises() {
		return paisServiceDao.findAllPaises();
	}

	@Override
	public Pais findById(Long codigoPais) {
		return paisServiceDao.findById(codigoPais);
	}

	@Override
	public Pais findByDescripcion(String descripcion) {
		return paisServiceDao.findByDescripcion(descripcion);
	}
}
