package com.lab.core.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import com.lab.core.model.Producto;
import com.lab.core.service.ProductoService;


@Controller
@RequestMapping("/api/v1")

public class ProductoController {
	@Autowired
	private ProductoService productoService;
	@RequestMapping(value="/producto",method= RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<List<Producto>> getProductos(
			@RequestParam(value="nombre",
			required = false)String nombre){
		List<Producto> producto = new ArrayList<Producto>();
		if(nombre != null) {
			Producto elemento = productoService.findByNombre(nombre);
			if(elemento == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}else {
				producto.add(elemento);
			}
		}else {
			producto = productoService.findAllProductos();
			if(producto.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
		}
		return new ResponseEntity<List<Producto>>(producto,HttpStatus.OK);
	}
	//GET FINDBYID
	@RequestMapping(value="/producto/{id}",method=RequestMethod.GET,headers="Accept=application/json")
		public ResponseEntity<Producto> getProductoById(@PathVariable("id")Long id){
			Producto elemento = productoService.findById(id);
			if(elemento == null) {
				return new ResponseEntity<Producto>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Producto>(elemento,HttpStatus.OK);
	}	
	//POST
	@RequestMapping(value="/producto",method = RequestMethod.POST,headers="Accept=application/json")
	public ResponseEntity<?> createProducto(@RequestBody
			Producto producto,UriComponentsBuilder ucBuilder){
		if(productoService.findByNombre(producto.getNombre())!=null){
		return new ResponseEntity("Existe un producto con este nombre",HttpStatus.CONFLICT);
		
	}
	productoService.saveProducto(producto);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder
			.path("api/v1/producto/{id}")
			.buildAndExpand(producto.getCodigoProducto())
			.toUri());
	return new ResponseEntity<String>(headers,HttpStatus.CREATED);
	}
	//DELETE
	@RequestMapping(value="/producto/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProducto(@PathVariable("id") Long id){
		if(id == null || id <= 0) {
			return new ResponseEntity("Debe ingresar un id",HttpStatus.CONFLICT);
		}
		Producto elemento = productoService.findById(id);
		if(elemento == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		productoService.deleteProductoById(elemento.getCodigoProducto());
		return new ResponseEntity<Producto>(HttpStatus.OK);	
	}
	
	//UPDATE
	@RequestMapping(value="/producto/{id}", method = RequestMethod.PUT,headers="Accept=application/json")
	public ResponseEntity<Producto> updateProducto(@PathVariable("id") Long id,@RequestBody Producto producto){
		if(id == null || id <=0) {
			return new ResponseEntity("Debe Ingresar un id",HttpStatus.CONFLICT);
		}
		Producto elemento = productoService.findById(id);
		if(elemento == null) {
			return new ResponseEntity("",HttpStatus.NO_CONTENT);
		}
		elemento.setNombre(producto.getNombre());
		productoService.updateProducto(elemento);
		return new ResponseEntity<Producto>(elemento,HttpStatus.OK);
	}
}
 
