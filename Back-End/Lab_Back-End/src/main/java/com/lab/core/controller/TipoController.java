package com.lab.core.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import com.lab.core.model.Pais;
import com.lab.core.model.Tipo;
import com.lab.core.service.PaisService;
import com.lab.core.service.TipoService;


@Controller
@RequestMapping("/api/v1")

public class TipoController {
	@Autowired
	private TipoService tipoService;
	@RequestMapping(value="/tipo",method= RequestMethod.GET,headers="Accept=application/json")
	public ResponseEntity<List<Tipo>> getTipos(
			@RequestParam(value="descripcion",
			required = false)String descripcion){
		List<Tipo> tipo = new ArrayList<Tipo>();
		if(descripcion != null) {
			Tipo elemento = tipoService.findByDescripcion(descripcion);
			if(elemento == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}else {
				tipo.add(elemento);
			}
		}else {
			tipo = tipoService.findAllTipos();
			if(tipo.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
		}
		return new ResponseEntity<List<Tipo>>(tipo,HttpStatus.OK);
	}
	//GET FINDBYID
	@RequestMapping(value="/tipo/{id}",method=RequestMethod.GET,headers="Accept=application/json")
		public ResponseEntity<Tipo> getTipoById(@PathVariable("id")Long id){
			Tipo elemento = tipoService.findById(id);
			if(elemento == null) {
				return new ResponseEntity<Tipo>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Tipo>(elemento,HttpStatus.OK);
	}	
	//POST
	@RequestMapping(value="/tipo",method = RequestMethod.POST,headers="Accept=application/json")
	public ResponseEntity<?> createTipo(@RequestBody
			Tipo tipo,UriComponentsBuilder ucBuilder){
		if(tipoService.findByDescripcion(tipo.getDescripcion())!=null){
		return new ResponseEntity("Existe un tipo con esta descripcion",HttpStatus.CONFLICT);
		
	}
	tipoService.saveTipo(tipo);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder
			.path("api/v1/tipo/{id}")
			.buildAndExpand(tipo.getCodigoTipo())
			.toUri());
	return new ResponseEntity<String>(headers,HttpStatus.CREATED);

	}
	//DELETE
	@RequestMapping(value="/tipo/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTipo(@PathVariable("id") Long id){
		if(id == null || id <= 0) {
			return new ResponseEntity("Debe ingresar un id",HttpStatus.CONFLICT);
		}
		Tipo elemento = tipoService.findById(id);
		if(elemento == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		tipoService.deleteTipoById(elemento.getCodigoTipo());
		return new ResponseEntity<Tipo>(HttpStatus.OK);	
	}
	
	//UPDATE
	@RequestMapping(value="/tipo/{id}", method = RequestMethod.PUT,headers="Accept=application/json")
	public ResponseEntity<Tipo> updateTipo(@PathVariable("id") Long id,@RequestBody Tipo tipo){
		if(id == null || id <=0) {
			return new ResponseEntity("Debe Ingresar un id",HttpStatus.CONFLICT);
		}
		Tipo elemento = tipoService.findById(id);
		if(elemento == null) {
			return new ResponseEntity("",HttpStatus.NO_CONTENT);
		}
		elemento.setDescripcion(tipo.getDescripcion());
		tipoService.updateTipo(elemento);
		return new ResponseEntity<Tipo>(elemento,HttpStatus.OK);
	}
}
 
