package com.lab.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Laboratorio1 {

	public static void main(String[] args) {
		SpringApplication.run(Laboratorio1.class, args);
		System.out.println("Laboratorio1");
	}
}
