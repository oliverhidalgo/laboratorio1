package com.lab.core.controller;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lab.core.model.Pais;
import com.lab.core.service.PaisService;

@Controller
public class MainController {
	@Autowired
	private PaisService paisService;
	@RequestMapping("/")
	@ResponseBody
	public String index() {
		String response = "<h1>Laboratorio 1</h1>";
		
		return response;
	}
}
